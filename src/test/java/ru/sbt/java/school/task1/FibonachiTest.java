package ru.sbt.java.school.task1;

import org.junit.Test;

import static org.junit.Assert.*;

public class FibonachiTest {

    @Test
    public void calculate() {

        // assert statements
        assertEquals(0, Fibonachi.calculate(0));
        assertEquals(1, Fibonachi.calculate(1));
        assertEquals(1, Fibonachi.calculate(2));
        assertEquals(3, Fibonachi.calculate(5));
        assertEquals(34, Fibonachi.calculate(10));
        assertEquals(514229, Fibonachi.calculate(30));
    }
}