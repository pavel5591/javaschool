package ru.sbt.java.school.task1;

public class Fibonachi {
    public static int calculate(int value)
    {
        if(value == 0) return 0;
        if(value == 1) return 1;
        return calculate(value -2) + calculate(value -1);
    }
}
