package ru.sbt.java.school.task1;

import java.util.logging.Logger;
import java.util.Scanner;

public class Main {
    static {
        System.setProperty("log4j.configuration", "file:src/main/recources/log4j.properties");
    }

    final static Logger logger = Logger.getLogger("Main.class");

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Введите значение: ");
        String input = in.nextLine();
        int count = Integer.parseInt(input);
        for(int i = 0; i < count; i++)
            logger.info("Число: " + Fibonachi.calculate(i));
    }
}
